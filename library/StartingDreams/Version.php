<?php
/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Version
 * 
 * Version information for both StartingDreams and PHP.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Version {
    
    /**
     * Package major version
     */
    const major = 0;
    
    /**
     * Package minor version
     */
    const minor = 1;
    
    /**
     * Pagkage build  version
     */
    const build = 0;

    /**
     * StartingDreams Version information
     * @return string
     */
    public static function version() {
        return implode('.', array(self::major, self::minor, self::build));
    }
    
    /**
     * Extract php major.minor.release version information.
     * @return string
     */
    public static function phpVersion() {
        return implode('.', array(PHP_MAJOR_VERSION, PHP_MINOR_VERSION, PHP_RELEASE_VERSION));
    }
}
