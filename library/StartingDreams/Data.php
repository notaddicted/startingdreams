<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Data
 * 
 * Adds ability to set and get data to objects.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
trait Data {
    
    /**
     * @var array
     */
    private $data = array();

    
    /**
     * If $name is an array then $name is assumed to be an array of data to set.
     */
    public function setData($name, $value = null) {       
        
        if(!is_array($name)) {
            $method = 'set' . ucfirst($name);
            if(method_exists($this, $method)) {
                return $this->$method($value);
            }
            $this->data[$name] = $value;
            return $this;
        }
        
        foreach($name as $key => $value) {
            $this->__set($key, $value);
        }
        
        return $this;
    }
    
    /**
     * Pull values from the $data array
     */
    public function getData($name = null) {
        if(null === $name) {
            return $this->data;
        }
        
        $method = 'get' . ucfirst($name);
        if(method_exists($this, $method)) {
            return $this->$method();
        }
        
        return $this->data[$name];
    }
    
    /**
     * Magic getter for $data values
     */
    public function __get($name) {
        return $this->getData($name);
    }
    
    /**
     * Magic setter for $data values
     * 
     * If data is already set and both values are associative arrays then they are merged.
     * If not, the new value replaces the old.
     * 
     */
    public function __set($name, $value) {
        if(isset($this->data[$name]) && is_array($this->data[$name]) && $this->isAssociativeArray($this->data[$name])) {
            if(is_array($value) && $this->isAssociativeArray($value)) {
                $this->setData($name, array_merge($this->data[$name], $value));
                return $this;
            }
        }

        $this->setData($name, $value);
        return $this;
    }
    
    /**
     * Magic isset for $data values
     */
    public function __isset($name) {
        return isset($this->data[$name]);
    }
    
    /**
     * Magic unset for $data values
     */
    public function __unset($name) {
        unset($this->data[$name]);
    }
    
    /**
     * Tests to see if an array is associative. 
     * Note: Only checks to see if at least one string key exists.
     */
    protected function isAssociativeArray($array) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }
}
