<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Exception
 * 
 * Extends php Exception.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * @todo    Add development mode and logging to exceptions 
 * 
 */
class Exception extends \Exception{
    
}
