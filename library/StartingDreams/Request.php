<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Request
 * 
 * Client request information.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Request {
    use \StartingDreams\Data;
    
    protected $host;
    
    protected $path;
    
    protected $method;
    
    protected $get;
    
    protected $post;
    
    protected $public_directory;

    /**
     * Sets starting Data;
     * 
     * @param type $data 
     * @throws \StartingDreams\Exception if passed anything other than an array.
     */
    public function __construct($data = array()) {
        if(!is_array($data)) {
            throw new \Exception('Data must be an array');
        }
        
        $this->setDefaults();
        $this->setData($data);
    }
    
    /**
     * @return string
     */
    public function getPublicDirectory() {
        return $this->public_directory;
    }
    
    /**
     * @param string $value
     * @return \StartingDreams\Request
     */
    public function setPublicDirectory($value) {
        $this->public_directory = $value;
        return $this;
    }
    
    public function setPath($value) {
        $this->path = $value;
        return $this;
    }
    
    public function getPath() {
        return $this->path;
    }
    
    public function setPost($value) {
        $this->post = $value;
        return $this;
    }
    
    public function getPost() {
        return $this->post;
    }
    
    public function setGet($value) {
        $this->get = $value;
        return $this;
    }
    
    public function getGet() {
        return $this->get;
    }
    
    public function setHost($value) {
        $this->host = $value;
        return $this;
    }
    
    public function getHost() {
        return $this->host;
    }
    
    
    public function setMethod($value) {
        $this->method = $value;
        return $this;
    }
    
    public function getMethod() {
        return $this->method;
    }
    
    /**
     * Set dafault settings.
     */
    protected function setDefaults() {
        $this->setData(array(
            'host' => $_SERVER['HTTP_HOST'],
            'path' => $_SERVER['REQUEST_URI'],
            'method' => $_SERVER['REQUEST_METHOD'],
            'get'  => $_GET,
            'post' => $_POST,
        ));
    }

}
