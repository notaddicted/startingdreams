<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Autoloader
 * 
 * Autoloads StartingDreams classes.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Autoloader {
    
    /**
     * Register autoloader.
     */
    public static function registerAutoloader() {
        spl_autoload_register(__NAMESPACE__ . "\\Autoloader::load");
    }
    
    /**
     * Load class by passed name
     * @param type $className
     */
    public static function load($className) {
        $className = str_replace(__NAMESPACE__, '', $className);
        $className = ltrim($className, "\\_");
        
        $filename = __DIR__ . DIRECTORY_SEPARATOR . str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, $className) . '.php';
        
        if(file_exists($filename)) {
            require_once $filename;
        }
    }
}
