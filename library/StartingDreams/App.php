<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * App
 * 
 * Main application class. Proccesses routes and settings.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class App {
    
    /**
     * Application config settings.
     * @var \StartingDreams\Config
     */
    protected $config;
    
    /**
     * Application router
     * @var \StartingDreams\Router 
     */
    protected $router;
    
    /**
     * @var bool
     */
    protected $debug;

    /**
     * Force singleton pattern
     */
    private function __construct() {}
    
    
    /**
     * Returns the application instance.
     * @return \StartingDreams\App
     */
    public static function getInstance() {
        
        static $instance = null;
        if(null === $instance) {
            $instance = new static();
        }
        return $instance;
        
    }
    
    /**
     * Runs application
     * @param \StartingDreams\Config $config
     */
    public function run(\StartingDreams\Config $config = null) {
        
        if(null !== $config) {
            $this->setConfig($config);
        }
        
        $this->setRouter(new \StartingDreams\Router($this->getConfig()));
        $this->getRouter()->route();
    }
    
    
    /**
     * @param  \StartingDreams\Config $config
     * @return \StartingDreams\App
     */
    public function setConfig(\StartingDreams\Config $config) {
        $this->config = $config;
        return $this;
    }
    
    /**
     * @return \StartingDreams\Config
     */
    public function getConfig() {
        return $this->config;
    }
    
    /**
     * @param  \StartingDreams\Router $router
     * @return \StartingDreams\App
     */
    public function setRouter(\StartingDreams\Router $router) {
        $this->router = $router;
        return $this;
    }

    /**
     * @return \StartingDreams\Router
     */
    public function getRouter() {
        return $this->router;
    }
    
    
    public function setDebug($value) {
        $this->debug = (bool)$value;
    }
    
    public function getDebug() {
        return $this->debug;
    }
    
    protected function debugSettings() {
        if(!$this->getDebug()) {
            return $this;
        }
        
        ini_set('display_errors', 'On');
        error_reporting(E_ALL);
    }
}
