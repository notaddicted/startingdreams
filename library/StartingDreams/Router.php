<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Router
 * 
 * Controls Routing.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Router {
    use \StartingDreams\Data;
    
    /**
     * @var array of routes
     */
    protected $routes;
    
    /**
     * @var \StartingDreams\Request
     */
    protected $request;

    public function __construct(\StartingDreams\Config $config) {
        $this->routes = array();
        
        if(!isset($config->request)) {
            throw new \StartingDreams\Exception('Router does not have missing request object.');
        }
        
        $this->setRequest($config->request);
        
        foreach($config->routes as $name => $data) {
            $this->createRoute($name, $data);
        }
    }
    
    public function setRequest(\StartingDreams\Request $value) {
        $this->request = $value;
        return $this;
    }
    
    public function getRequest() {
        return $this->request;
    }
    
    protected function createRoute($name, $data) {
        if(isset($data['class'])) {
            $route = $this->default_route = new $data['class']($name, $data);
        } else {
            $route = new \StartingDreams\Router\Route($name, $data);
        }
        return $this->addRoute($route);
    }
    
    /**
     * @param \StartingDreams\Router\Route $route
     */
    public function addRoute(\StartingDreams\Router\Route $route) {
        $this->routes[$route->getName()] = $route;
        return $this;
    }
    
    
    /**
     * @param array $routes
     */
    public function addRoutes($routes) {
        if(!is_array($routes)) {
            $routes = array($routes);
        }
        
        foreach($routes as $route) {
            $this->addRoute($route);
        }
        return $this;
    }

    
    public function route() {
        $path = $this->getRequest()->getPath();
        foreach($this->routes as $route) {
            if($route->match($path)) {
                $route->dispatch($this->getRequest());
                return $this;
            }
        }
        $this->getDefaultRouter()->dispatch($this->getRequest());
        return $this;
    }
}
