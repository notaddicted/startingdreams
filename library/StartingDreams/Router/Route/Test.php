<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams\Router\Route;

/**
 * Test
 * 
 * Test route.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Test extends \StartingDreams\Router\Route {
        
}
