<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams\Router\Route;

/**
 * DefaultRoute
 * 
 * route action when the route is unknown.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class DefaultRoute extends \StartingDreams\Router\Route {

    public function dispatch(\StartingDreams\Request $request) {
        $this->setResponse($this->loadFile($this->getData('filename'), $request));
        parent::dispatch($request);
    }
}
