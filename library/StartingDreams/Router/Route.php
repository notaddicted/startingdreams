<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams\Router;

/**
 * Route
 * 
 * Creates an instance of the passed class name and calls the function passed as
 * "action". Additionaly, config data and the router is passed to the class.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Route {
    use \StartingDreams\Data;
    
    /**
     * @var string route name
     */
    protected $name;

    /**
     * @var string route path
     */
    protected $path;
        
    /**
     * Name of the function called on the class
     * @var string 
     */
    protected $action;
    
    /**
     * Name of class to create
     * @var string
     */
    protected $class;
    
    
    protected $response;


    /**
     * @param string $name
     * @param array $data
     */
    public function __construct($name, $data) {
        if(empty($name) || !is_string($name)) {
            throw new \StartingDreams\Exception('Cannot create Route: invalid name');
        }
        if(!is_array($data) || !isset($data['path'])) {
            throw new \StartingDreams\Exception('Cannot create Route ' . $name . ': data missing');
        }
        
        $this->setName($name);
        
        $this->setData($data);
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * @param string $value
     * @return \StartingDreams\Router\Route
     */
    public function setName($value) {
        $this->name = $value;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPath() {
        return $this->path;
    }
    
    /**
     * @param string $value
     * @return \StartingDreams\Router\Route
     */
    public function setPath($value) {
        $this->path = $value;
        return $this;
    }
    
    public function setResponse($response) {
        $this->response = $response;
    }
    
    public function getResponse() {
        return $this->getResponse();
    }
    

    /**
     * @param  \StartingDreams\Config $config
     * @throws \StartingDreams\Exception
     */
    public function isValid($data) {
        if(empty($data['name'])) {
           throw new \StartingDreams\Exception('Route name missing');
        }
        if(empty($data['path'])) {
           throw new \StartingDreams\Exception('Route path missing');
        }

        $this->name = $config->name;
        $this->path = $config->path;
    }
    
    public function dispatch(\StartingDreams\Request $request) {
        echo "dispatched: " . $this->getName() . '<br/>';
        echo "class: " . get_class($this);
        echo $this->response;
    }
    
    /**
     * @param  string $filename
     * @return string
     * @throws \StartingDreams\Exception
     */
    public function loadFile($filename, $request) {
        
        $filenameWithPath = $request->getPublicDirectory() . DIRECTORY_SEPARATOR . str_replace(array('\\', '_'), DIRECTORY_SEPARATOR, $filename);
        
        if(!file_exists($filenameWithPath)) {
            throw new \StartingDreams\Exception('File not found: ' . $filename);
        }
        
        $file = file_get_contents($filenameWithPath);
        
        return $file;
    }
    
    public function match($path) {
        $length = strlen($this->getPath());
        if(substr($path, 0, $length) == $this->getPath()) {
            return true;
        }
        return false;
    }
}
