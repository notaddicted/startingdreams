<?php

/**
 * Starting Dreams - a lightweight php framework for Thick Fontends.
 * 
 * @author David Smith <david@startingdreams.com>
 * @package StartingDreams
 * 
 * Very simple framework designed to support a Thick Frontend. 
 */

namespace StartingDreams;

/**
 * Config
 * 
 * Stores config information for the package.
 * 
 * @package StartingDreams
 * @author  David Smith
 * @since   0.1.0
 * 
 */
class Config {
    use \StartingDreams\Data;

    /**
     * Sets starting Data;
     * 
     * @param type $data 
     * @throws \StartingDreams\Exception if passed anything other than an array.
     */
    public function __construct($data = array()) {
        if(!is_array($data)) {
            throw new \Exception('Data must be an array');
        }
        
        $this->setDefaults();
        $this->setData($data);
    }
    
    /**
     * Set dafault settings.
     */
    protected function setDefaults() {
        $this->setData(array(
            'debug' => true,
            'routes' => array(
                '/api/test' => array(
                    'class' => 'StartingDreams\\Router\\Route\\Test', 
                    'path' => '/api/test'
                ),
                'defaultRoute' => array(
                    'name' => 'default',
                    'class' => 'StartingDreams\\Router\\Route\\DefaultRoute',
                    'filename' => 'angularjs.html',
                    'path' => '',
                ),
            ),

        ));

    }

}
