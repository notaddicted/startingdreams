<?php 
/**
 * Load StartingDreams autoloader.
 */
require '../library/StartingDreams/Autoloader.php';
\StartingDreams\Autoloader::registerAutoloader();

/**
 * Create config and save settings.
 */
$config = new \StartingDreams\Config();
$config->request = new \StartingDreams\Request(array('publicDirectory' => __DIR__));


/**
 * Pass configuration to new app and run.
 */
$app = \StartingDreams\App::getInstance();
$app->run($config);
?>